var express = require('express');
var router = express.Router();
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
const mongoose = require('mongoose')
var User = require('../models/user');

// Register
router.get('/register', function(req, res) {
    res.render('register');
});

// Login
router.get('/login', function(req, res) {
    res.render('login');
});

// Register User
router.post('/register', function(req, res) {
    var name = req.body.name;
    var email = req.body.email;
    var CPF = req.body.CPF;
    var password = req.body.password;
    var password2 = req.body.password2;

    // validacao
    req.checkBody('name', 'Preencha o campo nome').notEmpty();
    req.checkBody('email', 'Preencha o campo de e-mail').notEmpty();
    req.checkBody('email', 'O e-mail não é válido').isEmail();
    req.checkBody('CPF', 'Preencha o campo CPF de usuário').notEmpty();
    req.checkBody('password', 'Preencha o campo senha').notEmpty();
    req.checkBody('password2', 'As senhas não são iguais').equals(req.body.password);

    var errors = req.validationErrors();

    if (errors) {
        res.render('register', {
            errors: errors
        });
    } else {
        //verificando se email e usuario ja foram usados
        User.findOne({
            CPF: {
                "$regex": "^" + CPF + "\\b",
                "$options": "i"
            }
        }, function(err, user) {
            User.findOne({
                email: {
                    "$regex": "^" + email + "\\b",
                    "$options": "i"
                }
            }, function(err, mail) {
                if (user || mail) {
                    res.render('register', {
                        user: user,
                        mail: mail
                    });
                } else {
                    var newUser = new User({
                        name: name,
                        email: email,
                        CPF: CPF,
                        password: password
                    });
                    User.createUser(newUser, function(err, user) {
                        if (err) throw err;
                        console.log(user);
                    });
                    req.flash('success_msg', 'Condômino registrado com sucesso!');
                    res.redirect('/users/login');
                }
            });
        });
    }
});

passport.use(new LocalStrategy(
    function(CPF, password, done) {
        User.getUserByUsername(CPF, function(err, user) {
            if (err) throw err;
            if (!user) {
                return done(null, false, { message: 'CPF inválido' });
            }

            User.comparePassword(password, user.password, function(err, isMatch) {
                if (err) throw err;
                if (isMatch) {
                    return done(null, user);
                } else {
                    return done(null, false, { message: 'Senha inválida' });
                }
            });
        });
    }));

passport.serializeUser(function(user, done) {
    done(null, user.id);
});

passport.deserializeUser(function(id, done) {
    User.getUserById(id, function(err, user) {
        done(err, user);
    });
});

router.post('/login',
    passport.authenticate('local', { successRedirect: '/', failureRedirect: '/users/login', failureFlash: true }),
    function(req, res) {
        res.redirect('/');
    });

router.get('/logout', function(req, res) {
    req.logout();

    req.flash('success_msg', 'Você está desconectado');

    res.redirect('/users/login');
});

module.exports = router;