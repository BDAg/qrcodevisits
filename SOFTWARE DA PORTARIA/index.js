const express = require('express');
const app = express()
var path = require('path')
var exphbs = require('express-handlebars')
var routes = require('./controllers/routers');

app.set('views', path.join(__dirname, 'views'));
app.engine('handlebars', exphbs());
app.set('view engine', 'handlebars');

app.use(express.static(path.join(__dirname, 'public')));
app.use('/', routes)


PORT = 2001
app.listen(PORT, () => {
    console.log('SOFTWARE da portaria rodando na porta: ' + PORT)
})