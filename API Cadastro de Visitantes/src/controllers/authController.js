const express = require('express');
const User = require('../models/user')
const router = express.Router();
const bcrypt = require('bcryptjs');

router.post('/registro', async(req, res) => {
    try {
        const user = await User.create(req.body)
        return user.save().then(
            res.redirect('https://visitorcode.herokuapp.com')
        )
    } catch (err) {
        return res.status(400).send({ error: 'falha no registro' })
    }
})

router.post('/visitou', async(req, res) => {
    const { CPF } = req.body;
    const user = await User.findOne({ CPF });

    if (!user)
        return res.status(400).send({ error: 'Ops, seu cpf não está no banco de visitas.' })
    res.send({ user })
})

module.exports = app => app.use('/auth', router);