const mongoose = require('../database');
const bcrypt = require('bcryptjs');


const VisitorSchema = new mongoose.Schema({
    nome: {
        type: String,
        require: true,
    },
    houseID: {
        type: String,
        require: true,
    },
    numDeVisitantes: {
        type: Number,
        require: false,
    },
    CPF: {
        type: String,
        require: true,
        select: false,
        unique: true,
    },
    cidade: {
        type: String,
        require: true,
    },
    corCarro: {
        type: String,
        require: true,
    },
    data: {
        type: Date,
        default: Date.now
    }
});

const User = mongoose.model('User', VisitorSchema);

module.exports = User