var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var exphbs = require('express-handlebars');
var expressValidator = require('express-validator');
var flash = require('connect-flash');
var session = require('express-session');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var mongo = require('mongodb');
var mongoose = require('mongoose');

// conexao com mongoDB
// caso queira usar localhost, descomente a linha abaixo
//mongoose.connect('mongodb://localhost/autoroof');

// conexao via mlab com mongoDB
// caso queira usar localhost, comente a linha de conexao com o mongo
// precisa settar o usuario, senha e DB de acordo com o que foi criado no mlab
//mongoose.connect('mongodb://<usuario>:<senha>@ds151393.mlab.com:51393/<db_name>');
mongoose.connect('mongodb+srv://Jackie:japan1945@cluster0-w1rnf.mongodb.net/test?retryWrites=true', { useNewUrlParser: true });
var db = mongoose.connection;

var routes = require('./routes/index');
var users = require('./routes/users');

var app = express();

// views
app.set('views', path.join(__dirname, 'views'));
app.engine('handlebars', exphbs({ defaultLayout: 'layout' }));
app.set('view engine', 'handlebars');

// BodyParser Middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

// pasta estatica
app.use(express.static(path.join(__dirname, 'public')));

// session
app.use(session({
    secret: 'secret',
    saveUninitialized: true,
    resave: true
}));

// passport init
app.use(passport.initialize());
app.use(passport.session());

// validacao
app.use(expressValidator({
    errorFormatter: function(param, msg, value) {
        var namespace = param.split('.'),
            root = namespace.shift(),
            formParam = root;

        while (namespace.length) {
            formParam += '[' + namespace.shift() + ']';
        }
        return {
            param: formParam,
            msg: msg,
            value: value
        };
    }
}));

// Connect Flash
app.use(flash());

// variaveis globais
app.use(function(req, res, next) {
    res.locals.success_msg = req.flash('success_msg');
    res.locals.error_msg = req.flash('error_msg');
    res.locals.error = req.flash('error');
    res.locals.user = req.user || null;
    next();
});

app.use('/', routes);
app.use('/users', users);

// settando a porta
app.listen(process.env.PORT || 7077)