const express = require('express');
const User = require('../models/user')
const router = express.Router();
const bcrypt = require('bcryptjs');

const Gpio = require('onoff').Gpio; //include onoff to interact with the GPIO
const LED = new Gpio(18, 'out'); //use GPIO pin 4, and specify that it is output



router.post('/registro', async(req, res) => {
    try {
        const user = await User.create(req.body)
        return user.save().then(() => {
            res.redirect('http://localhost:3000/')
        })
    } catch (err) {
        return res.status(400).send({ error: 'falha no registro' })
    }
})

router.post('/Visitou', async(req, res) => {
    const { CPF } = req.body;
    const user = await User.findOne({ CPF });
    if (!user)
        return res.status(400).send({ error: 'Ops, seu cpf não está no banco de visitas.' })
    blinkLED();
    
    
function blinkLED() { //function to start blinking
    if (LED.readSync() === 0) { //check the pin state, if the state is 0 (or off)
        LED.writeSync(1); //set pin state to 1 (turn LED on)
        setTimeout(blinkLED, 10000); 
        res.redirect('http://localhost:2001')
        
        
  } else {
    LED.writeSync(0); //set pin state to 0 (turn LED off)
    
  }
}

})


    

module.exports = app => app.use('/auth', router);
